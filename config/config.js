var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'test';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'seedproductlisting'
    },
    db: 'mongodb://localhost:27017/products',
    debug: true
  },

  test: {
    root: rootPath,
    app: {
      name: 'seedproductlisting'
    },
    db: 'mongodb://api:12api34@ds119768.mlab.com:19768/productlisting',
    debug: true
  },

  production: {
    root: rootPath,
    app: {
      name: 'seedproductlisting'
    },
    db: 'mongodb://api:12api34@ds119768.mlab.com:19768/productlisting',
    debug: false
  }
};

module.exports = config[env];
