//////////////////////////////////
//
//	JOB Interface.
//  Plz implement this on each new Job
//
//////////////////////////////////

var config = require('../../config/config');

var IJob = function() {
	'use strict'

	var IJob = {
		'run': run
	}
	return IJob;

	//////////

	function run() {
		return true;
	}
};

// Commenting so we'll not get the scope dirty with dummy interface
//module.exports = IJob;