var _productsService = require('../services/productsService'),
	originProducts = require('../../data/products');


var seedJob = function() {
	'use strict'

	var productsService = new _productsService();
	var products = [];

	var seedJob = {
		'run': run
	}
	return seedJob;

	//////////

	function run() {
		saveProducts();
	}

	function saveProducts() {
		originProducts.map(function(product){
			product.date = new Date();
			product.link = "";
			delete product['_id'];

			if(product.image.indexOf('multiplus') == -1) {
				console.log('Start saving product ' + product.name.substr(0, 25));
				productsService.insert(
					product,
					function(obj) {
						console.log('Done saving ' + obj.name);
					}
				);
			}
		});
	}
};

module.exports = seedJob;