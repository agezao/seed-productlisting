var mongoose = require('mongoose'),
    Products = mongoose.model('Products');

var productsService = function() {
	'use strict'

	var productsService = {
		'insert': insert
	}
	return productsService;

	//////////

	function insert(viewModel, callback) {
		var obj = new Products(viewModel);

		obj.save(function(err){
			if(err)
				if(callback)
					return callback(err);

			if(callback)
				callback(obj);
		});
	}
};

module.exports = productsService;













