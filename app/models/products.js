var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductsSchema = new Schema({
    barcode:  {type: String},
    name:  {type: String, required: true},
    price:  {type: Number, required: true},
    description:  {type: String},
    image:  {type: String},
    supplier:  {type: String},
    category:  {type: String},
    link:  {type: String},
	date: {type: Date, required: true}
});


module.exports = mongoose.model('Products', ProductsSchema);