var config   = require('./config/config'),
    glob     = require('glob'),
    mongoose = require('mongoose');    

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});

seedJob = require("./app/jobs/seed");

var run = function(){
  console.log('Triggering seed');

    var _seedJob = new seedJob();
    _seedJob.run();

  console.log('Finished Triggering seed');
};

console.log('Seeding started!');
run();