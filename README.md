# Seed - Product Listing#

This project is in charge of populating the database for the application.

Build up with Node.js and the seed data is provided on /data/products.js

By default the application is seeding the database at MongoLab, but feel free to change this at /config/config.js if you wish

### ** How to run ** ###

* Clone or download the project
* npm install
* run npm start and wait until it finishes

![App Architecture - Seed.png](https://bitbucket.org/repo/d4rkLB/images/234078238-App%20Architecture%20-%20Seed.png)